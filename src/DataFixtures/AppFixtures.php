<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $categories = [];

        for ($i = 0; $i < 10; $i++) {
            $category = new Category();
            $category->setName($faker->sentence($nbWords = 1, $variableNbWords = false));

            $manager->persist($category);
            $categories[] = $category;
        };

        for ($x = 0; $x < 50; $x++) {
            $product = new Product();
            $product->setName($faker->sentence($nbWords = 3, $variableNbWords = true))
                ->setDescription($faker->sentence($nbWords = 15, $variableNbWords = true))
                ->setPrice($faker->numberBetween(5, 200));
            $number = $faker->numberBetween(0, 4);
            for ($y = 0; $y < $number; $y++) {
                $product->addCategory(
                    $categories[$faker->numberBetween(0, count($categories) - 1)]);
            };
            $manager->persist($product);
        };


        $manager->flush();
    }
}
